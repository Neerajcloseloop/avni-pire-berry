import React from 'react';
import { useSelector } from 'react-redux';
// mui
import {ThemeProvider, useTheme } from '@mui/material/styles';
// next
import dynamic from 'next/dynamic';

// project
import themes from '../../src/themes';
import MissionsList from '../../src/components/missions-list';

const MainLayout = dynamic(() => import('../../src/layout/MainLayout'), {
    ssr: false
});


export default function Missions() {
    const theme = useTheme();
    const customization = useSelector((state) => state.customization);
    return (
        <ThemeProvider theme={themes(customization)}>
            <MainLayout>
               <MissionsList type={1} />
            </MainLayout>
        </ThemeProvider>
    );
}
