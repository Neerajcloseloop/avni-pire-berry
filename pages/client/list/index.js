import React, {useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
// mui
import { ThemeProvider, useTheme } from '@mui/material/styles';
import RenderTable  from '../../../src/components/client/table';
import { gridSpacing } from '../../../src/store/constant';
import ClientService from '../../../src/services/client.service';
import dynamic from 'next/dynamic';

import { Grid, Card, Box, Typography, Button} from '@mui/material';
import Breadcrumbs from '../../../src/ui-component/extended/Breadcrumbs';
import ClientAddModal from '../../../src/components/client-add-modal';
import { IconChevronRight } from '@tabler/icons';
// import clientArray from '../../../src/_mockApis/client.json';
// project
import themes from '../../../src/themes';

const MainLayout = dynamic(() => import('../../../src/layout/MainLayout'), {
    ssr: false
});

const Clients = (props) => {
    const theme = useTheme();
    const customization = useSelector((state) => state.customization);
    const [clients, setClients] = useState([]);
    const [sort, setSort] = useState({ column: 'name', order: 1 });
    const [totalItems, setTotalItems] = useState(0);
    const [isModalOpen, setModalState] = useState(false);
    const clientService = new ClientService();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(25);
    
    const getClientData = async () => {
      let clientsData = await clientService.getClients();
      clientsData.sort((a, b) => {
        return a[sort.column].toLowerCase() > b[sort.column].toLowerCase() ? sort.order : sort.order * -1;
      });
      setTotalItems(clientsData.length);
      clientsData = clientsData.slice((page - 1) * pageSize, page * pageSize);
      setClients(clientsData);
      return clientsData
    }
    useEffect(() => {
        getClientData();
      //setClients(clientArray);
    }, []);
  
 
  
    const handleSortChange = (event, column) => {
      const orderValue = sort.column === column && sort.order === 1 ? -1 : 1;
      setSort({ column: column, order: orderValue });
    };
  
    const handleSearchChange = (event) => {
      setSearch(event.target.value);
    };



    // table header
    const headCells = [      
        {
            id: 'name',
            numeric: false,
            disablePadding: false,
            label: 'CLIENT'
        },
      
    ];

    const TableHeader1 = () => {
      return (
          <Card
              sx={{
                  marginBottom: theme.spacing(gridSpacing),
                  border: '1px solid',
                  borderColor: theme.palette.background.default,
                  background: theme.palette.background.default
              }}
          >
              <Box sx={{ p: 2, pl: 2 }}>
                  <Grid
                      container
                      direction={'row'}
                      justifyContent={'space-between'}
                      alignItems={'center'}
                      spacing={1}
                  >
                      <Grid item>
                          <Typography variant="h3" sx={{ fontWeight: 500 }}>
                              {'Clients'}
                          </Typography>
                      </Grid>
                      <Breadcrumbs separator={IconChevronRight} navigation={'test'} icon title rightAlign />
                     
                  </Grid>

              </Box>
          </Card>)
  }
  return (
      <ThemeProvider theme={themes(customization)}>
      <MainLayout>
      {<ClientAddModal type={1} title ="Add Client" clientOptions={[]} isOpen={isModalOpen} setClose={()=> setModalState(!isModalOpen)}
              handleAddClient={()=>{}} />}
          <Grid container spacing={gridSpacing}>              
              <Grid item xs={12} md={12} lg={12}>
                {<TableHeader1 />}
                <RenderTable headerText={ ""} data={clients} onSort={handleSortChange} onButtonClick={()=> setModalState(!isModalOpen)} columns={headCells} onSearch={handleSearchChange} />
              </Grid>
          </Grid>
          </MainLayout>
      </ThemeProvider>
  );
}

export default Clients;