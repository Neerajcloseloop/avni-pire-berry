import React, {useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
// mui
import { ThemeProvider, useTheme } from '@mui/material/styles';
import { gridSpacing } from '../../../src/store/constant';
import ClientService from '../../../src/services/client.service';
import dynamic from 'next/dynamic';

import { Grid, Card, Box, TextField, Button, DialogActions,  CardActions, Divider,} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
// project
import themes from '../../../src/themes';
import EditTabs from  './editTabs';

import AnimateButton from '../../../src/ui-component/extended/AnimateButton';
import SaveIcon from '@mui/icons-material/Save';
// import clientDetailObject from '../../../src/_mockApis/client-apijson.json';
const MainLayout = dynamic(() => import('../../../src/layout/MainLayout'), {
    ssr: false
});

export default function ClientDetails() {
    const theme = useTheme();
    const customization = useSelector((state) => state.customization);
    const router = useRouter();

    const [client, setClientData] = useState({
        clientId: 0,
        name: ''
      });
      const [clientName, setClientName] = useState('');
    const [mode, setMode] = useState('view');
    const [id, setId] = useState(0);
    const clientService = new ClientService();


    useEffect(() => {
        const clientId = router.query.id;
        const getClient = async (id) => {
            const client = await clientService.getClient(id);
            setClientName(client.name)
            setClientData(client);
        };

        if (clientId !== 0 || clientId !== undefined) {
            getClient(clientId);
            setId(clientId);
        }
    }, []);

    useEffect(() => {
        setMode('view')
    }, [])

    const handleBackClick = ()=>{
        if (history.length === 1) {
            router.push(`/clients`);
          } else {
            router.back();
          }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log('client click', client)
        const dataToSave = {...client, name: clientName}
        if (!clientName) {
          //setErrors(clientValidator.errors);
        } else {
          let response = false;
          if (client.clientId === 0) {
            response = await clientService.addClient(dataToSave);
          } else {
            response = await clientService.updateClient(dataToSave);
          }
          if (response) {
            router.push(`/client/list`);
          }
        }
      };

    const TableHeader1 = () => {
      return (
          <Card
              sx={{
                  marginBottom: theme.spacing(gridSpacing),
                  border: '1px solid',
                  borderColor: theme.palette.background.default,
                  background: theme.palette.background.default
              }}
          >
              <Box sx={{ p: 2, pl: 2 }}>
                  <Grid
                      container
                      direction={'row'}
                      justifyContent={'space-between'}
                      alignItems={'center'}
                      spacing={1}
                  >
                      <Grid item   xs={12} sx={{display : 'flex'}}>
                          <div xs={6} style={{ fontWeight: 'Bold', marginRight: '40px', display: 'flex', width: '30%', alignItems: 'center' }}>
                              <span>{'Client'} :  </span>
                          </div>
                          {mode === 'view' ? <span style={{marginLeft: '25px'}}> {clientName} </span>
                              :                              
                              <TextField
                                  fullWidth
                                  value={clientName}
                                  label="client name"
                                  variant="standard"
                                  error={false}
                                  onChange={(event)=> {setClientName(event.target.value)}}
                                 // helperText={client.name.length <= 0 && 'Please enter valid client name'}
                              />}
                          </Grid>
                          <Divider /> 
                          <Grid item   xs={12} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                          <DialogActions >
                              <Button onClick={handleBackClick} variant="contained" style={{ marginTop: 'none !important', marginBottom: '0px !important', backgroundColor: '#6c757d', borderColor: '#6c757d' }} sx={{ my: 3, ml: 1 }}>
                              Back
                              </Button>
                            
                              <AnimateButton>
                                  <Button onClick={(event) => mode === 'view' ? setMode('edit') : handleSubmit(event) } 
                                  startIcon={mode === 'view' ? <EditIcon /> : <SaveIcon />} 
                                  sx={{ marginBottom: '0px !important',  my: 3, ml: 1  }} variant="contained" type="submit" >
                                    {mode === 'view' ? 'Edit' : 'Save'}
                                  </Button>
                              </AnimateButton>
                          </DialogActions>
                      </Grid>
                     
                  </Grid>

              </Box>
          </Card>)
  }
  return (
      <ThemeProvider theme={themes(customization)}>
          <MainLayout>
              <Grid container spacing={gridSpacing}>
                  <Grid item xs={12} md={12} lg={12}>
                      {<TableHeader1 />}
                      <Card
                          sx={{
                              marginBottom: theme.spacing(gridSpacing),
                              border: '1px solid',
                              borderColor: theme.palette.background.default,
                              background: theme.palette.background.default
                          }}
                      >
                          <Box sx={{ p: 2, pl: 2 }}>
                              <EditTabs clientData={client} />
                          </Box>
                      </Card>
                  </Grid>
              </Grid>
          </MainLayout>
      </ThemeProvider>
  );
}