import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Box, Tab, Tabs, Typography } from '@mui/material';

import ClientService from '../../../src/services/client.service';
import MissionsList from '../../../src/components/missions-list';

// tab content customize
const TabPanel = ({ children, value, index, ...other }) => {
    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

const EditTabs = ({clientData = {}}) => {
    const theme = useTheme();
    const [value, setValue] = React.useState(0);
console.log('clientData clientData', clientData)
    const clientService = new ClientService();
    const [client, setClient] = useState({
      clientId: 0,
      name: '',
      domains: [],
      groups: [],
      missions: []
    });
    const [tab, setTab] = useState(1);
  
    useEffect(() => {
        setClient(clientData);
    }, [clientData]);

    const handleChange = (event, newValue)=> {
        setValue(newValue);
    };

    return (
        <>
            <Tabs
                value={value}
                onChange={handleChange}
                textColor="secondary"
                indicatorColor="secondary"
                sx={{
                    mb: 3,
                    '& a': {
                        minHeight: 'auto',
                        minWidth: 10,
                        py: 1.5,
                        px: 1,
                        mr: 2.2,
                        color: theme.palette.grey[600],
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center'
                    },
                    '& a.Mui-selected': {
                        color: theme.palette.primary.main
                    },
                    '& a > svg': {
                        mb: '0px !important',
                        mr: 1.1
                    }
                }}
            >
                <Tab
                    component={''}
                    to="#"
                    label="Group"
                />
                <Tab
                    component={''}
                    to="#"
                    label="Missions"
                />
                 <Tab
                    component={''}
                    to="#"
                    label="Domains"
                />
            </Tabs>
            <TabPanel value={value} index={0}>
             {/* <GroupTable groups={client?.groups} /> */}
            </TabPanel>
            <TabPanel value={value} index={1}>
              {/* <MissionTable missions={client?.missions} /> */}
              <MissionsList type={4}  missions={client?.missions}/>
            </TabPanel>
            <TabPanel value={value} index={2}>
            {/* <DomainTable domains={client?.domains} /> */}
            </TabPanel>
          
        </>
    );
}

export default EditTabs;