import React from 'react';
import { useSelector } from 'react-redux';

// next
import dynamic from 'next/dynamic';

// mui
import { ThemeProvider, useTheme } from '@mui/material/styles';
import { CardContent, Grid, Card, Box, Typography } from '@mui/material';

import InfoSharpIcon from '@mui/icons-material/InfoSharp';
import MuiTooltip, { tooltipClasses, TooltipProps } from '@mui/material/Tooltip';

// project imports
import themes from '../../src/themes';
import MainCard from '../../src/ui-component/cards/MainCard';
import { gridSpacing } from '../../src/store/constant';
import MissionsList  from '../../src/components/missions-list'
import BoxBarChart from '../../src/components/chart/box-bar-chart'; 


import MissionActivity from '../../src/components/mission-activity';
import { LineChart } from '../../src/components/chart/line-chart';

const MainLayout = dynamic(() => import('../../src/layout/MainLayout'), {
    ssr: false
});

const textToDisplay= "Sample text to display ample text to display ample text to display  "


export default function Home() {
    const customization = useSelector((state) => state.customization);
    const theme = useTheme();
    return (
        <ThemeProvider theme={themes(customization)}>
            <MainLayout>
                <Card
                    sx={{
                        marginBottom: theme.spacing(gridSpacing),
                        border: '1px solid',
                        borderColor: theme.palette.background.default,
                        background: theme.palette.background.default
                    }}
                >
                    <Box sx={{ p: 2, pl: 2 }}>
                        <Grid
                            container
                            direction={'row'}
                            justifyContent={'space-between'}
                            alignItems={'center'}
                            spacing={1}
                        >
                            <Grid item>
                                <Typography variant="h3" sx={{ fontWeight: 500 }}>
                                    {'Welcome to PIRE'}
                                </Typography>
                            </Grid>

                        </Grid>
                    </Box>

                </Card>               
                <Grid container spacing={gridSpacing}>
                    {3===4 &&<><Grid item xs={12} md={7} lg={7}>
                        <MainCard title="test block 01" content={false}>
                          <CardContent> <div>test block 01</div></CardContent>
                        </MainCard>
                    </Grid>
                    <Grid item xs={12} md={5} lg={5}>
                        <MainCard title="test block 02" content={false}>
                        <CardContent>  <div>test block 01</div></CardContent>
                        </MainCard>
                    </Grid>
                    <Grid item xs={12} md={7} lg={6}>
                        <MainCard title="test block 03" content={false}>
                        <CardContent>  <div>test block 01</div></CardContent>
                        </MainCard>
                    </Grid>
                    <Grid item xs={12} md={5} lg={6}>
                        <MainCard title="test block 04" content={false}>
                        <CardContent>  <div>test block 01</div></CardContent>
                        </MainCard>
                    </Grid>
                    </>}
                    <Grid item xs={12} md={12} lg={12}>
                        <MissionsList type={2} />
                    </Grid>
                    <Grid item xs={12} md={5}>
                        <Grid>
                        <MainCard title="PIRE System Status" content={false}
                            secondary={<MuiTooltip title={textToDisplay} arrow placement="left-start" >
                                <InfoSharpIcon style={{
                                    color: 'rebeccapurple',
                                    cursor: 'pointer',
                                }} /></MuiTooltip>}>
                            <CardContent> 
                              {<LineChart />}
                            </CardContent>
                        </MainCard>
                        </Grid>
                        <Grid sx={{marginTop:'10px'}}>
                        <MainCard title="Pire Scheduled for next 7 days" content={false}
                            secondary={<MuiTooltip title={textToDisplay} arrow placement="left-start" >
                                <InfoSharpIcon style={{
                                    color: 'rebeccapurple',
                                    cursor: 'pointer',
                                }} /></MuiTooltip>}>
                            <CardContent> <BoxBarChart /> </CardContent>
                        </MainCard>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={7}>
                         <MissionActivity title="All Pire Missions - Activity" />                        
                    </Grid>
                </Grid>
            </MainLayout>
        </ThemeProvider>
    );
}
