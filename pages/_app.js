import React from 'react';
import { Provider } from 'react-redux';

// mui
import { CssBaseline, StyledEngineProvider } from '@mui/material';

// project import
import { store } from '../src/store';
import Locales from '../src/ui-component/Locales';
import RTLLayout from '../src/ui-component/RTLLayout';
import Snackbar from '../src/ui-component/extended/Snackbar';
import NavigationScroll from '../src/layout/NavigationScroll';
import '../src/_mockApis';
import '../src/styles/scss/style.scss';
import { AppContext, useAppState } from '../src/context/app.context';

export default function MyApp({ Component, pageProps }) {
    const { state, actions } = useAppState();
    return (
        <Provider store={store}>
            <StyledEngineProvider injectFirst>
                <CssBaseline />
                <RTLLayout>
                    <Locales>
                        <NavigationScroll>
                        <AppContext.Provider value={{ state, actions }}>
                                <Component {...pageProps} />
                                <Snackbar />
                            </AppContext.Provider>
                        </NavigationScroll>
                    </Locales>
                </RTLLayout>
            </StyledEngineProvider>
        </Provider>
    );
}
