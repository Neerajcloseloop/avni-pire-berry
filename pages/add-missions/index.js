import React, {useEffect, useState} from 'react';
import { useSelector } from 'react-redux';
// mui
import { ThemeProvider, useTheme } from '@mui/material/styles';
// next
import dynamic from 'next/dynamic';

// project
import themes from '../../src/themes';
// material-ui
import { Button, Checkbox, FormControlLabel, Grid, Stack, TextField, Autocomplete } from '@mui/material';

// project imports
import AnimateButton from '../../src/ui-component/extended/AnimateButton';

// third-party
import { useFormik } from 'formik';
import * as yup from 'yup';

import Link from 'next/link';
import { useRouter } from 'next/router';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCloudDownloadAlt, faPaperclip, faPen, faPlus, faSave } from '@fortawesome/free-solid-svg-icons';
//import ClientModal from '../client/client.modal';
// import DomainModal from '../domain/domain.modal';
// import GroupModal from '../group/group.modal';
import ClientService from '../../src/services/client.service';
import DomainService from '../../src/services/domain.service';
import GroupService from '../../src/services/group.service';
import LocationService from '../../src/services/location.service';
import MissionService from '../../src/services/mission.service';
import SearchEngineDefinitionService from '../../src/services/search-engine-definition.service';
import MissionValidator from '../../src/validators/mission.validator';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import SaveIcon from '@mui/icons-material/Save';
import SubCard from '../../src/ui-component/cards/SubCard';


const validationSchema = yup.object({
    cardName: yup.string().required('First Name is required'),
    cardNumber: yup.string().required('Last Name is required')
});
import MainCard from '../../src/ui-component/cards/MainCard';

const MainLayout = dynamic(() => import('../../src/layout/MainLayout'), {
    ssr: false
});


export default function AddMissions({id= 0}) {
    const theme = useTheme();
    const customization = useSelector((state) => state.customization);
    const [errorIndex, setErrorIndex] = React.useState(null);

    const router = useRouter();
    const clientService = new ClientService();
    const domainService = new DomainService();
    const groupService = new GroupService();
    const locationService = new LocationService();
    const missionService = new MissionService();
    const searchEngineDefinitionService = new SearchEngineDefinitionService();
    const missionValidator = new MissionValidator();

    const [clients, setClients] = useState([]);
    const [groups, setGroups] = useState([]);
    const [domains, setDomains] = useState([]);
    const [searchEngineDefinitions, setSearchEngineDefinitions] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [locations, setLocations] = useState([]);
    const [scheduleDayValues, setScheduleDayValues] = useState([]);
    const [scheduleHourValues, setScheduleHourValues] = useState([]);
    const [mission, setMission] = useState({
        missionId: 0,
        name: '',
        clientId: 0,
        groupId: 0,
        location: 'United States',
        criteriaId: 2840,
        volumeCriteriaId: 0,
        maxResult: 100,
        schedule: '',
        scheduleUnit: 1,
        scheduleDay: 1,
        scheduleHour: 1,
        missionType: 'Both',
        priority: false,
        searchEngineDefinitions: [],
        domains: [],
        searchTerms: [],
        fileAttachment: null,
        asinFileAttachment: null,
    });
    const [errors, setErrors] = useState({
        name: '',
        clientId: '',
        groupId: '',
        location: '',
        volumeCriteriaId: '',
        maxResult: '',
        schedule: '',
        scheduleDay: '',
        scheduleHour: '',
        missionType: '',
        priority: 0,
        searchEngineDefinitions: '',
        domains: '',
        fileAttachment: '',
        asinFileAttachment: '',
        errors: []
    });
    const [isValidated, setIsValidated] = useState(false);
    const [locationDisplay, setLocationDisplay] = useState(true);

    useEffect(() => {
        const getSearchEngineDefinitions = async (mission = null) => {
            const searchEngineDefinitions = await searchEngineDefinitionService.getSearchEngineDefinitions();
            searchEngineDefinitions.sort((a, b) => (a.searchEngine.name.toLowerCase() > b.searchEngine.name.toLowerCase() ? 1 : -1));
            searchEngineDefinitions.forEach((x) => {
                if (mission.searchEngineDefinitions.filter((y) => y.searchEngineDefinitionId === x.searchEngineDefinitionId).length > 0) {
                    x.checked = true;
                    x.original = true;
                } else {
                    x.checked = false;
                    x.original = false;
                }
            });

            setSearchEngineDefinitions(searchEngineDefinitions);
        };

        const getMission = async (id) => {
            const mission = await missionService.getMission(id);
            setMission(mission);

            getClients();
            getSearchEngineDefinitions(mission);
        };

        if (id !== 0) {
            getMission(id);
        } else {
            getClients();
            getSearchEngineDefinitions(mission);
        }
    }, [id]);

    useEffect(() => {
        if (mission.clientId !== '') {
            getGroups();
            getDomains();
        }
    }, [mission.clientId]);

    useEffect(() => {
        const scheduleDayValues = missionService.getScheduleDayValues(mission.schedule);
        const scheduleHourValues = missionService.getScheduleHourValues();
        setScheduleDayValues(scheduleDayValues);
        setScheduleHourValues(scheduleHourValues);
    }, [mission.schedule]);

    useEffect(() => {
        if (isValidated) {
            missionValidator.isValid(mission);
            setErrors(missionValidator.errors);
        }
    }, [
        mission.name,
        mission.clientId,
        mission.groupId,
        mission.location,
        mission.volumeCriteriaId,
        mission.maxResult,
        mission.schedule,
        mission.scheduleUnit,
        mission.scheduleDay,
        mission.scheduleHour,
        mission.missionType,
        mission.priority,
        mission.searchEngineDefinitions,
        mission.domains,
        mission.fileAttachment,
        mission.asinFileAttachment,
    ]);

    const getClients = async () => {
        const clients = await clientService.getClients();
        clients.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1));
        setClients(clients);
    };

    const getDomains = async () => {
        const domains = await domainService.getDomainsByClient(mission.clientId);
        domains.sort((a, b) => (a.domainUrl.toLowerCase() > b.domainUrl.toLowerCase() ? 1 : -1));
        domains.forEach((x) => {
            if (mission.domains.filter((y) => y.domainId === x.domainId).length > 0) {
                x.checked = true;
                x.original = true;
            } else {
                x.checked = false;
                x.original = false;
            }
        });
        setDomains(domains);
    };

    const getGroups = async () => {
        const groups = await groupService.getGroupsByClient(mission.clientId);
        groups.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1));
        setGroups(groups);
    };

    const handleBack = () => {
        if (history.length === 1) {
            router.push(`/mission`);
        } else {
            router.back();
        }
    };

    const handleChange = (event) => {
        setMission({
            ...mission,
            [event.target.name]: event.target.value
        });
    };

    const handleAutocompleteChange = (query) => {
        const getLocations = async (query) => {
            const locations = await locationService.getLocationsAutocomplete(query);
            setLocations(locations);
            setIsLoading(false);
        };

        setIsLoading(true);
        getLocations(query);
    };

    const handleCheckboxChange = (event, id, name, checkbox) => {
        let options = mission[name];
        checkbox.checked = event.target.checked;
        if (event.target.checked) {
            options = options.concat(checkbox);
        } else {
            options = options.filter((x) => x[id] !== checkbox[id]);
        }
        setMission({
            ...mission,
            [name]: options
        });
    };

    const handleLocationChange = (value) => {
        setMission({
            ...mission,
            criteriaId: value[0]?.criteriaId ? value[0]?.criteriaId : 0,
            location: value[0]?.canonicalName ? value[0]?.canonicalName : ''
        });
        setLocationDisplay(true);
    };

    const handleLocationDisplay = (event) => {
        setLocationDisplay(false);
    };

    const handleFileChange = (event) => {
        if (event.target.files && event.target.files.length > 0) {
            setMission({
                ...mission,
                fileAttachment: event.target.files[0]
            });
        }
    };

    const handleAsinFileChange = (event) => {
        if (event.target.files && event.target.files.length > 0) {
            setMission({
                ...mission,
                asinFileAttachment: event.target.files[0]
            });
        }
    };

    const handleDownload = (event) => {
        event.preventDefault();
        missionService.downloadKeywords(mission);
    };

    const handleSubmit = async () => {
        console.log('test save click===============>')
        // event.preventDefault();
        // if (!missionValidator.isValid(mission)) {
        //     setErrors(missionValidator.errors);
        // } else {
        //     let response = null;
        //     if (mission.missionId === 0) {
        //         response = await missionService.addMission(mission);
        //         if (response) {
        //             for (const searchEngineDefinition of searchEngineDefinitions) {
        //                 if (searchEngineDefinition.checked) {
        //                     const missionSearchEngineDefinition = {
        //                         missionId: response.missionId,
        //                         searchEngineDefinitionId: searchEngineDefinition.searchEngineDefinitionId
        //                     };
        //                     await missionService.addMissionSearchEngineDefinition(missionSearchEngineDefinition);
        //                 }
        //             }

        //             for (const domain of domains) {
        //                 if (domain.checked) {
        //                     const missionDomain = {
        //                         missionId: response.missionId,
        //                         domainId: domain.domainId
        //                     };
        //                     await missionService.addMissionDomain(missionDomain);
        //                 }
        //             }

        //             if (mission.fileAttachment !== undefined && mission.fileAttachment !== null) {
        //                 await missionService.uploadMissionSearchTerms(response.missionId, mission.fileAttachment);
        //             }
        //         }
        //     } else {
        //         const data = { ...mission };
        //         data.searchTerms = [];
        //         response = await missionService.updateMission(data);

        //         for (const searchEngineDefinition of searchEngineDefinitions) {
        //             if (searchEngineDefinition.checked === true && searchEngineDefinition.original === false) {
        //                 const missionSearchEngineDefinition = {
        //                     missionId: response.missionId,
        //                     searchEngineDefinitionId: searchEngineDefinition.searchEngineDefinitionId
        //                 };
        //                 await missionService.addMissionSearchEngineDefinition(missionSearchEngineDefinition);
        //             }

        //             if (searchEngineDefinition.checked === false && searchEngineDefinition.original === true) {
        //                 const missionSearchEngineDefinition = {
        //                     missionId: response.missionId,
        //                     searchEngineDefinitionId: searchEngineDefinition.searchEngineDefinitionId
        //                 };
        //                 await missionService.deleteMissionSearchEngineDefinition(missionSearchEngineDefinition);
        //             }
        //         }

        //         for (const domain of domains) {
        //             if (domain.checked === true && domain.original === false) {
        //                 const missionDomain = {
        //                     missionId: response.missionId,
        //                     domainId: domain.domainId
        //                 };
        //                 await missionService.addMissionDomain(missionDomain);
        //             }

        //             if (domain.checked === false && domain.original === true) {
        //                 const missionDomain = {
        //                     missionId: response.missionId,
        //                     domainId: domain.domainId
        //                 };
        //                 await missionService.deleteMissionDomain(missionDomain);
        //             }
        //         }
        //         if (mission.fileAttachment !== undefined && mission.fileAttachment !== null) {
        //             await missionService.uploadMissionSearchTerms(response.missionId, mission.fileAttachment);
        //         }
        //         if (mission.asinFileAttachment !== undefined && mission.asinFileAttachment !== null) {
        //             await missionService.uploadMissionAsins(response.missionId, mission.asinFileAttachment);
        //         }
        //     }
        //     if (response) {
        //         if (history.length === 1) {
        //             router.push(`/mission`);
        //         } else {
        //             router.back();
        //         }
        //     }
        // }
        // setIsValidated(true);
    };

    const formik = useFormik({
        initialValues: {
            cardName: '',
            cardNumber: ''
        },
        validationSchema,
        onSubmit: (values) => {
            setPaymentData({
                cardName: values.cardName,
                cardNumber: values.cardNumber
            });
            handleNext();
        }
    });

    const kwOptions = [
        { id: 0, label: 'Global' },
        { id: 2840, label: 'United States' },
        { id: 2124, label: 'Canada' },
        { id: 2156, label: 'China' },
        { id: 2356, label: 'India' },
        { id: 2484, label: 'Mexico' },
        { id: 2826, label: 'United Kingdom' }]

    const scheduleOptions = [
        { id: 'Once', label: 'Once' },
        { id: 'Daily', label: 'Daily' },
        { id: 'Weekly', label: 'Weekly' },
        { id: 'Monthly', label: 'Monthly' }
    ]

    const missionTypeOptions = [
        { id: 'Both', label: 'Ranking / Volume' },
        { id: 'Ranking', label: 'Ranking Only' },
        { id: 'Volume', label: 'Volume Only' }
    ]

    return (
        <ThemeProvider theme={themes(customization)}>
            <MainLayout>
                <MainCard title='Add Missions' secondary={<Grid item xs={12}>
                    <Stack direction="row" justifyContent="space-between">
                        <AnimateButton>
                            <Button onClick={()=>handleDownload()} startIcon={<CloudDownloadIcon />} sx={{marginTop: 'none !important', marginBottom: '0px !important'}} variant="contained" type="submit" sx={{ my: 3, ml: 1 }} onClick={() => setErrorIndex(1)}>
                            Download Keyword Template
                            </Button>
                        </AnimateButton>                        
                        <AnimateButton>
                            <Button onClick={()=>handleSubmit()} startIcon={<SaveIcon />} sx={{marginTop: 'none !important', marginBottom: '0px !important'}} variant="contained" type="submit" sx={{ my: 3, ml: 1 }} onClick={() => setErrorIndex(1)}>
                                Save
                            </Button>
                        </AnimateButton>
                        <Button onClick={()=>handleBack()} variant="contained"  style={{marginTop: 'none !important', marginBottom: '0px !important', backgroundColor: '#6c757d', borderColor: '#6c757d'}} onClick={() => { }} sx={{ my: 3, ml: 1 }}>
                            Cancel
                        </Button>
                    </Stack>
                </Grid>}>
                    <form onSubmit={formik.handleSubmit}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    id="mission"
                                    name="mission"
                                    value={formik.values.mission}
                                    onChange={formik.handleChange}
                                    error={formik.touched.mission && Boolean(formik.errors.mission)}
                                    helperText={formik.touched.mission && formik.errors.mission}
                                    label="Mission"
                                    fullWidth
                                />
                            </Grid>
                            <Grid sx={{display: 'flex', flexDirection: 'row', alignItems: 'center'}} item xs={12} md={6}>
                                <div className='col-12 col-md-3 col-lg-2' style={{ display: 'flex' }}>
                                    <label className='font-weight-bold text-muted' htmlFor='location'>
                                        Location : 
                                    </label>                                                                       
                                </div>
                                {
                                    !locationDisplay ?
                                        <AsyncTypeahead
                                            style={{width: '100%',  lineHeight: '20px', borderRadius: '10px'}}
                                            id='locationAutocomplete'
                                            name='locationAutocomplete'
                                            isLoading={isLoading}
                                            labelKey='canonicalName'
                                            onChange={handleLocationChange}
                                            onSearch={handleAutocompleteChange}
                                            options={locations}
                                            placeholder='Find Location...'
                                            searchText='Searching...'
                                        />
                                        : <div style={{marginLefft: '40px', display : 'flex', alignItems: 'center', flexDirection : 'row', justifyContent: 'space-between'}}>
                                            {locationDisplay === true && <span>{mission.location}</span>}
                                            <div style={{marginLeft: '10px'}}>
                                                <button type='button' className={`btn btn-primary btn-sm ${locationDisplay === false ? 'd-none' : ''}`} onClick={() => handleLocationDisplay()}>
                                                    <FontAwesomeIcon icon={faPen} />
                                                </button>
                                            </div>
                                        </div>
                                }                
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Autocomplete
                                    disablePortal
                                    options={clients.length > 0
                                        ? clients.map((client, index) => {
                                           return{
                                               id: client.clientId,
                                               label: client.name
                                           }
                                        })
                                        : []}
                                    renderInput={(params) => <TextField {...params} label="Client" />}
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Autocomplete
                                    disablePortal
                                    options={groups.length > 0
                                        ? groups.map((grp, index) => {
                                           return{
                                               id: grp.groupId,
                                               label: grp.name
                                           }
                                        })
                                        : []}
                                    renderInput={(params) => <TextField {...params} label="Group" />}
                                />
                            </Grid>
                           
                            <Grid item xs={12}>
                                <Autocomplete
                                    disablePortal
                                    id="volumeCriteriaId"
                                    name="volumeCriteriaId"
                                    options={kwOptions}
                                    renderInput={(params) => <TextField {...params} label="KWV Country" />}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="maxResult"
                                    name="maxResult"
                                    label="Max Results"
                                    value={mission.maxResult}
                                    onChange={formik.handleChange}
                                    error={formik.touched.maxResult && Boolean(formik.errors.maxResult)}
                                    helperText={formik.touched.maxResult && formik.errors.maxResult}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Autocomplete
                                    disablePortal
                                    options={scheduleOptions}
                                    renderInput={(params) => <TextField {...params} label="Schedule" />}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Autocomplete
                                    disablePortal
                                    id='scheduleHour'
                                    name='scheduleHour'
                                    options={scheduleHourValues.length > 0
                                        ? scheduleHourValues.map((scheduleHourValue, index) => {
                                            return { id: scheduleHourValue.value, label: scheduleHourValue.text }

                                        })
                                        : []}
                                    defaultValue={mission.scheduleHour}
                                    renderInput={(params) => <TextField {...params} label="Hour" />}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Autocomplete
                                    disablePortal
                                    id='missionType'
                                    name='missionType'
                                    options={missionTypeOptions}
                                    defaultValue={mission.missionType}
                                    renderInput={(params) => <TextField {...params} label="Mission Type" />}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Autocomplete
                                    disablePortal
                                    id='priority'
                                    name='priority'
                                    options={[{id: 'high', label: 'High'}, {id: 'normal', label: 'Normal'}]}
                                    renderInput={(params) => <TextField {...params} label="Priority" />}
                                />
                            </Grid>
                            <Grid item xs={12} md={12}>
                                <SubCard title="Search Engines">
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>
                                            <TextField
                                                id="location"
                                                name="location"
                                                label="Location"
                                                value={formik.values.location}
                                                onChange={formik.handleChange}
                                                error={formik.touched.location && Boolean(formik.errors.location)}
                                                helperText={formik.touched.location && formik.errors.location}
                                                fullWidth
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                id="location"
                                                name="location"
                                                label="Location"
                                                value={formik.values.location}
                                                onChange={formik.handleChange}
                                                error={formik.touched.location && Boolean(formik.errors.location)}
                                                helperText={formik.touched.location && formik.errors.location}
                                                fullWidth
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                id="location"
                                                name="location"
                                                label="Location"
                                                value={formik.values.location}
                                                onChange={formik.handleChange}
                                                error={formik.touched.location && Boolean(formik.errors.location)}
                                                helperText={formik.touched.location && formik.errors.location}
                                                fullWidth
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                id="location"
                                                name="location"
                                                label="Location"
                                                value={formik.values.location}
                                                onChange={formik.handleChange}
                                                error={formik.touched.location && Boolean(formik.errors.location)}
                                                helperText={formik.touched.location && formik.errors.location}
                                                fullWidth
                                            />
                                        </Grid>
                                    </Grid>
                                </SubCard>
                            </Grid>
                        </Grid>
                    </form>
                </MainCard>
            </MainLayout>
        </ThemeProvider>
    );
}
