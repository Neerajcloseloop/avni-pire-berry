import React from 'react';
import { useSelector } from 'react-redux';

// next
import dynamic from 'next/dynamic';

// mui
import { ThemeProvider } from '@mui/material/styles';

// project imports
import themes from '../src/themes';

const Home = dynamic(() => import('../pages/home/index'));
const PageLayout = dynamic(() => import('../src/layout'), { ssr: false });

function IndexPage() {
    const customization = useSelector((state) => state.customization);

    return (
        <>
            <ThemeProvider theme={themes(customization)}>
                <PageLayout>
                    <Home />
                </PageLayout>
            </ThemeProvider>
        </>
    );
}

export default IndexPage;
