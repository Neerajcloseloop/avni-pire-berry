// eslint-disable-next-line no-undef
module.exports = {
  trailingSlash: true,
  reactStrictMode: true,
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  images: {
    loader: 'akamai',
    path: 'http://pire-admin-berry-dev.s3-website-us-east-1.amazonaws.com',
  },
}
