import { createContext, useContext, useMemo, useState } from 'react';

const AppContext = createContext({});

const useAppState = () => {
  const initialState = {
    client: 0,
    group: 0
  };

  const [state, setState] = useState(initialState);
  const actions = useMemo(() => getActions(setState), [setState]);

  return { state, actions };
};

const getActions = (setState) => ({
  setClient: (clientId) => {
    setState((state) => ({ ...state, client: clientId }));
  },
  setGroup: (groupId) => {
    setState((state) => ({ ...state, group: groupId }));
  }
});

const useAppContext = () => {
  return useContext(AppContext);
};

export { AppContext, useAppState, useAppContext };
