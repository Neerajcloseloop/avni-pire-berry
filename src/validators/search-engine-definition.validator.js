import FormValidator from './form.validator';

export default class SearchEngineDefinitionValidator {
  formValidator = new FormValidator();
  errors = {
    searchEngineId: '',
    userAgentId: '',
    searchDefinition: '',
    searchType: '',
    searchDepth: '',
    parameters: '',
    country: ''
  };

  isSearchEngineIdValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.searchEngineId = 'Please select search engine.';
      isValid = false;
    }

    return isValid;
  }

  isUserAgentIdValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.userAgentId = 'Please select user agent.';
      isValid = false;
    }

    return isValid;
  }

  isSearchDefinitionValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.searchDefinition = 'Please enter Search Definition.';
      isValid = false;
    }

    return isValid;
  }

  isSearchTypeValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.searchType = 'Please enter Search Type.';
      isValid = false;
    }

    return isValid;
  }

  isSearchDepthValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.searchDepth = 'Please enter Search Depth.';
      isValid = false;
    }

    return isValid;
  }

  isCountryValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.country = 'Please enter Country.';
      isValid = false;
    }

    return isValid;
  }

  isValid(data) {
    let isValid = true;
    if (!this.isSearchEngineIdValid(data.searchEngineId)) {
      isValid = false;
    }

    if (!this.isUserAgentIdValid(data.userAgentId)) {
      isValid = false;
    }

    if (!this.isSearchDefinitionValid(data.searchDefinition)) {
      isValid = false;
    }

    if (!this.isSearchTypeValid(data.searchType)) {
      isValid = false;
    }

    if (!this.isSearchDepthValid(data.searchDepth)) {
      isValid = false;
    }

    if (!this.isCountryValid(data.country)) {
      isValid = false;
    }

    return isValid;
  }
}
