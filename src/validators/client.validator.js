import FormValidator from './form.validator';

export default class ClientValidator {
  formValidator = new FormValidator();
  errors = {
    name: ''
  };

  isNameValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.name = 'Please enter client.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 100)) {
      this.errors.name = 'Please enter client less than 100 characters.';
      isValid = false;
    }

    return isValid;
  }

  isValid(data) {
    let isValid = true;
    if (!this.isNameValid(data.name)) {
      isValid = false;
    }

    return isValid;
  }
}
