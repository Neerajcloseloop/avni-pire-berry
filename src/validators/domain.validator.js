import FormValidator from './form.validator';

export default class DomainValidator {
  formValidator = new FormValidator();
  errors = {
    domainUrl: '',
    clientId: ''
  };

  isUrlValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.domainUrl = 'Please enter url.';
      isValid = false;
    }

    if (!this.formValidator.stringLength(value, 0, 250)) {
      this.errors.domainUrl = 'Please enter url less than 250 characters.';
      isValid = false;
    }

    if (!this.formValidator.isUrl(value)) {
      this.errors.domainUrl = 'Please enter valid url.';
      isValid = false;
    }

    return isValid;
  }

  isClientIdValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.clientId = 'Please select client.';
      isValid = false;
    }

    return isValid;
  }

  isValid(data) {
    let isValid = true;
    if (!this.isUrlValid(data.domainUrl)) {
      isValid = false;
    }

    if (!this.isClientIdValid(data.clientId)) {
      isValid = false;
    }

    return isValid;
  }
}
