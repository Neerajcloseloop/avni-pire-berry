import isEmail from 'validator/lib/isEmail';
import isURL from 'validator/lib/isURL';

export default class FormValidator {
  isEmpty(value) {
    return value === undefined || value === null || value === '';
  }

  isAlpha(value) {
    value = this.toString(value);
    const stringRegex = RegExp(/^[A-Z]+$/i);

    return stringRegex.test(value);
  }

  isAlphanumeric(value) {
    value = this.toString(value);
    const stringRegex = RegExp(/^[0-9A-Z]+$/i);

    return stringRegex.test(value);
  }

  isArrayEmpty(value) {
    return (value === undefined || value.length == 0) ? true : false;
  }

  isDateGreater(value, compareValue) {
    if (compareValue === null) {
      compareValue = new Date();
      compareValue.setHours(0,0,0,0);
    }

    return value >= compareValue;
  }

  isEmail(value) {
    value = this.toString(value);

    return isEmail(value);
  }

  isNumeric(value) {
    value = this.toString(value);
    const stringRegex = RegExp(/^[+-]?([0-9]*[.])?[0-9]+$/);

    return stringRegex.test(value);
  }

  isUrl(value) {
    value = this.toString(value);
    return isURL(value, { require_protocol: false});
  }

  range(value, min, max) {
    value = this.toNumber(value);
    return value < min || value > max ? false : true;
  }

  required(value) {
    return this.isEmpty(value) ? false : true;
  }

  stringLength(value, min, max) {
    value = this.toString(value);

    return value.length < min || value.length > max ? false : true;
  }

  toNumber(value) {
    return this.isEmpty(value) ? -1 : Number(value);
  }

  toString(value) {
    return this.isEmpty(value) ? '' : String(value);
  }
}
