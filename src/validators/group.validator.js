import FormValidator from './form.validator';

export default class GroupValidator {
  formValidator = new FormValidator();
  errors = {
    name: '',
    clientId: ''
  };

  isNameValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.name = 'Please enter group.';
      isValid = false;
    }

    if (!this.formValidator.stringLength(value, 0, 100)) {
      this.errors.name = 'Please enter group less than 100 characters.';
      isValid = false;
    }

    return true;
  }

  isClientIdValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.clientId = 'Please select client.';
      isValid = false;
    }

    return isValid;
  }

  isValid(data) {
    let isValid = true;

    if (!this.isNameValid(data.name)) {
      isValid = false;
    }

    if (!this.isClientIdValid(data.clientId)) {
      isValid = false;
    }

    return isValid;
  }
}
