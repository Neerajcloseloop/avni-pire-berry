import FormValidator from './form.validator';

export default class UserAgentValidator {
  formValidator = new FormValidator();
  errors = {
    name: '',
    userAgentString: '',
    resolution: ''
  };

  isNameValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.name = 'Please enter client.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 100)) {
      this.errors.name = 'Please enter client less than 100 characters.';
      isValid = false;
    }

    return isValid;
  }

  isUserAgentStringValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.name = 'Please enter user agent string.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 250)) {
      this.errors.name = 'Please enter user agent string less than 250 characters.';
      isValid = false;
    }

    return isValid;
  }

  isResolutionValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.name = 'Please enter resolution.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 250)) {
      this.errors.name = 'Please enter resolution less than 250 characters.';
      isValid = false;
    }

    return isValid;
  }

  isValid(data) {
    let isValid = true;
    if (!this.isNameValid(data.name)) {
      isValid = false;
    }

    if (!this.isUserAgentStringValid(data.userAgentString)) {
      isValid = false;
    }

    if (!this.isResolutionValid(data.resolution)) {
    isValid = false;
    }

    return isValid;
  }
}
