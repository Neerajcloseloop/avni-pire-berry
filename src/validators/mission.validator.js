import FormValidator from './form.validator';

export default class MissionValidator {
  formValidator = new FormValidator();
  errors = {
    name: '',
    clientId: '',
    groupId: '',
    location: '',
    volumeCriteriaId: '',
    maxResult: '',
    schedule: '',
    scheduleDay: '',
    scheduleHour: '',
    missionType: '',
    priority: '',
    searchEngineDefinitions: '',
    domains: '',
    fileAttachment: ''
  };

  isNameValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.name = 'Please enter mission.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 100)) {
      this.errors.name = 'Please enter mission less than 100 characters.';
      isValid = false;
    }

    return isValid;
  }

  isClientIdValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.clientId = 'Please select client.';
      isValid = false;
    }

    return isValid;
  }

  isGroupIdValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.groupId = 'Please select group.';
      isValid = false;
    }

    return isValid;
  }

  isLocationValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.location = 'Please enter location.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 250)) {
      this.errors.location = 'Please enter location less than 250 characters.';
      isValid = false;
    }

    return isValid;
  }

  isVolumeCriteriaIdValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.volumeCriteriaId = 'Please select KWV Country.';
      isValid = false;
    }

    return isValid;
  }

  isMaxResultValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.maxResult = 'Please enter max result.';
      isValid = false;
    }

    if (!this.formValidator.range(value, 10, 100)) {
      this.errors.maxResult = 'Please enter max result between 10 and 100.';
      isValid = false;
    }

    return isValid;
  }

  isScheduleValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.schedule = 'Please select schedule.';
      isValid = false;
    }

    return isValid;
  }

  isScheduleDayValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.scheduleDay = 'Please select schedule day.';
      isValid = false;
    }

    return isValid;
  }

  isScheduleHourValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.scheduleDay = 'Please select schedule hour.';
      isValid = false;
    }

    return isValid;
  }

  isMissionTypeValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.missionType = 'Please select mission type.';
      isValid = false;
    }

    return isValid;
  }

  isPriorityValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.priority = 'Please select priority.';
      isValid = false;
    }

    return isValid;
  }

  isDomainsValid(value) {
    let isValid = true;
    if (this.formValidator.isArrayEmpty(value)) {
      this.errors.domains = 'Please select domain.';
      isValid = false;
    }

    return isValid;
  }

  isSearchEngineDefinitionsValid(value) {
    let isValid = true;
    if (this.formValidator.isArrayEmpty(value)) {
      this.errors.searchEngineDefinitions = 'Please select search engine.';
      isValid = false;
    }

    return isValid;
  }

  isFileAttachmentValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.fileAttachment = 'Please choose file attachment.';
      isValid = false;
    }

    return isValid;
  }

  isValid(data) {
    let isValid = true;
    if (!this.isNameValid(data.name)) {
      isValid = false;
    }
    if (!this.isClientIdValid(data.clientId)) {
      isValid = false;
    }
    if (!this.isGroupIdValid(data.groupId)) {
      isValid = false;
    }
    if (!this.isLocationValid(data.location)) {
      isValid = false;
    }
    if (!this.isVolumeCriteriaIdValid(data.volumeCriteriaId)) {
      isValid = false;
    }
    if (!this.isMaxResultValid(data.maxResult)) {
      isValid = false;
    }
    if (!this.isScheduleValid(data.schedule)) {
      isValid = false;
    }
    if (data.schedule === 'Monthly' || data.schedule === 'Weekly') {
      if (!this.isScheduleDayValid(data.scheduleDay)) {
        isValid = false;
      }
    }
    if (!this.isScheduleHourValid(data.scheduleHour)) {
      isValid = false;
    }
    if (!this.isMissionTypeValid(data.missionType)) {
      isValid = false;
    }
    if (!this.isPriorityValid(data.priority)) {
      isValid = false;
    }
    if (data.missionType !== 'Volume') {
      if (!this.isSearchEngineDefinitionsValid(data.searchEngineDefinitions)) {
        isValid = false;
      }
    }

    return isValid;
  }
}
