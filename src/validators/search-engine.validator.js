import FormValidator from './form.validator';

export default class SearchEngineValidator {
  formValidator = new FormValidator();
  errors = {
    name: '',
    country: ''
  };

  isNameValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.name = 'Please enter search engine.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 100)) {
      this.errors.name = 'Please enter search engine less than 100 characters.';
      isValid = false;
    }

    return isValid;
  }

  isCountryValid(value) {
    let isValid = true;
    if (!this.formValidator.required(value)) {
      this.errors.country = 'Please enter country.';
      isValid = false;
    }
    if (!this.formValidator.stringLength(value, 0, 100)) {
      this.errors.country = 'Please enter country less than 100 characters.';
      isValid = false;
    }

    return isValid;
  }

  isValid(data) {
    let isValid = true;
    if (!this.isNameValid(data.name)) {
      isValid = false;
    }

    if (!this.isCountryValid(data.country)) {
      isValid = false;
    }

    return isValid;
  }
}
