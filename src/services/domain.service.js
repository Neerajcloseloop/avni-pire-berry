import { API_URL } from '../constants/api.constant';

export default class DomainService {
  apiUrl = API_URL;
  errors = [];

  async getDomains() {
    try {
      let response = await fetch(`${this.apiUrl}/domain`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getDomainsByClient(id) {
    try {
      let response = await fetch(`${this.apiUrl}/domain/client/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getDomain(id) {
    try {
      let response = await fetch(`${this.apiUrl}/domain/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async addDomain(data) {
    try {
      let response = await fetch(`${this.apiUrl}/domain`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
      });
      if (response.status === 201) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async updateDomain(data) {
    try {
      let response = await fetch(`${this.apiUrl}/domain/${data.domainId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async deleteDomain(data) {
    try {
      let response = await fetch(`${this.apiUrl}/domain/${data.domainId}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }
}
