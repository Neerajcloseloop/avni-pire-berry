import { API_URL } from '../constants/api.constant';
import dailyJson from '../data/daily.json';
import monthlyJson from '../data/monthly.json';

export default class MissionService {
  apiUrl = API_URL;
  errors = [];

  async getMissions() {
    try {
      let response = await fetch(`${this.apiUrl}/mission`, {
        method: 'get',
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getMission(id) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/${id}`, {
        method: 'get',
      });
      if (response.status === 200) {
        return await response.json();
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async addMission(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      console.log(response);
      if (response.status === 201) {
        return await response.json();
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async updateMission(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/${data.missionId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      if (response.status === 200) {
        return await response.json();
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async deleteMission(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/${data.missionId}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }


  async startMission(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/start/${data.missionId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async stopMission(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/stop/${data.missionId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async retryMission(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/retry/${data.missionId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async completeMission(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/complete/${data.missionId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async addMissionDomain(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/domain`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      if (response.status === 201) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async deleteMissionDomain(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/domain/${data.missionId}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async addMissionSearchEngineDefinition(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/search-engine-definition`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      if (response.status === 201) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async deleteMissionSearchEngineDefinition(data) {
    try {
      let response = await fetch(`${this.apiUrl}/mission/search-engine-definition/${data.missionId}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async uploadMissionSearchTerms(id, fileAttachment) {
    try {
      let formData = new FormData();
      formData.append('file', fileAttachment, fileAttachment.name);

      let response = await fetch(`${this.apiUrl}/mission/upload/${id}`, {
        method: 'post',
        body: formData
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async uploadMissionAsins(id, fileAttachment) {
    try {
      let formData = new FormData();
      formData.append('file', fileAttachment, fileAttachment.name);

      let response = await fetch(`${this.apiUrl}/mission/upload-asin/${id}`, {
        method: 'post',
        body: formData
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  // Helper Functions
  getNextRunDateTime(schedule, scheduleDay, scheduleHour, stopMission) {
    let nextRunDateTime = null;
    let today = new Date();
    today.setUTCMinutes(0);
    today.setUTCSeconds(0);
    today.setUTCMilliseconds(0);
    if (stopMission === false) {
      switch (schedule) {
        case 'Daily':
          if (today.getUTCHours() > scheduleHour) {
            today.setUTCDate(today.getUTCDate() + 1);
            today.setUTCHours(scheduleHour);
          } else {
            today.setUTCHours(scheduleHour);
          }
          nextRunDateTime = today;
          break;
        case 'Weekly':
          if (today.getUTCDay() > (scheduleDay - 1)
            || (today.getUTCDay() === (scheduleDay - 1) && today.getUTCHours() > scheduleHour)) {
            today.setUTCDate(today.getUTCDate() + (7 - today.getUTCDay() + scheduleDay - 1));
          } else {
            today.setUTCDate(today.getUTCDate() + (scheduleDay - 1 - today.getUTCDay()));
          }
          today.setUTCHours(scheduleHour);
          nextRunDateTime = today;
          break;
        case 'Monthly':
          if ((today.getUTCDate() > scheduleDay)
            || (today.getUTCDate() === scheduleDay && today.getUTCHours() > scheduleHour)) {
            today.setUTCDate(scheduleDay);
            today.setUTCMonth(today.getUTCMonth() + 1);
          } else {
            today.setUTCDate(scheduleDay);
          }
          today.setUTCHours(scheduleHour);
          nextRunDateTime = today;
          break;
        default:
          nextRunDateTime = null;
          break;
      }
    }

    return nextRunDateTime;
  }

  getScheduleDayValue(schedule, scheduleDay) {
    let scheduleDayLookup = null;
    let today = new Date();
    today.setUTCMinutes(0);
    today.setUTCSeconds(0);
    today.setUTCMilliseconds(0);
    const timeZone = today.toLocaleTimeString('en-us',{timeZoneName:'short'}).split(' ')[2];
    switch (schedule) {
      case 'Weekly':
        scheduleDayLookup = monthlyJson.find(x => x.value === scheduleDay);
        break;
      case 'Monthly':
        scheduleDayLookup = monthlyJson.find(x => x.value === scheduleDay);
        break;
      default:
        scheduleDayLookup = null;
        break;
    }

    return scheduleDayLookup === null ? '' : scheduleDayLookup?.text;
  }

  getScheduleHourValue(scheduleHour) {
    let scheduleHourLookup = null;
    let today = new Date();
    today.setUTCMinutes(0);
    today.setUTCSeconds(0);
    today.setUTCMilliseconds(0);
    const timeZone = today.toLocaleTimeString('en-us',{timeZoneName:'short'}).split(' ')[2];
    dailyJson.map((daily, index) => {
      today.setUTCHours(index);
      daily.text = `${today.toLocaleTimeString()} ${timeZone} (${daily.timeText} UTC)`;
    });
    scheduleHourLookup = dailyJson.find(x => x.value === scheduleHour);

    return scheduleHourLookup === null ? '' : scheduleHourLookup?.text;
  }

  getScheduleDayValues(schedule) {
    let scheduleDayValues = [];
    switch (schedule) {
      case 'Weekly':
        scheduleDayValues = monthlyJson;
        break;
      case 'Monthly':
        scheduleDayValues = monthlyJson;
        break;
      default:
        scheduleDayValues = [];
        break;
    }

    return scheduleDayValues;
  }

  getScheduleHourValues() {
    let scheduleHourValues = [];

    const today = new Date();
    const timeZone = today.toLocaleTimeString('en-us', { timeZoneName: 'short' }).split(' ')[2];
    today.setUTCMinutes(0);
    today.setUTCSeconds(0);
    today.setUTCMilliseconds(0);
    dailyJson.map((daily, index) => {
      today.setUTCHours(index);
      daily.text = `${today.toLocaleTimeString()} ${timeZone} (${daily.timeText} UTC)`;
    });
    scheduleHourValues = dailyJson;

    return scheduleHourValues;
  }

  downloadKeywords(mission) {
    var csv = this.convertToCSV(mission.searchTerms);

    var exportedFilenmae = mission.name.replace(' ', '_') + '_keywords.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) {
      // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      const link = document.createElement('a');
      if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', exportedFilenmae);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  convertToCSV(searchTerms) {
    let str = '\uFEFFkeyword,branded,category 1,category 2,category 3,category 4,category 5,category 6,category 7,category 8,category 9,category 10' + '\r\n';
    const fields = ['searchTerm', 'branded', 'category1', 'category2', 'category3', 'category4', 'category5', 'category6', 'category7', 'category8', 'category9', 'category10'];
    for (let i = 0; i < searchTerms.length; i++) {
      let line = '';
      for (let j = 0; j < fields.length; j++) {
        if (line != '') line += ',';
        if (fields[j] === 'branded') {
          line += searchTerms[i][fields[j]] ? '1' : '0';
        } else {
          line += searchTerms[i][fields[j]];
        }
      }
      str += line + '\r\n';
    }
    return str;
  }
}
