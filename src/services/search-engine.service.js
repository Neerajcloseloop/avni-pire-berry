import { API_URL } from '../constants/api.constant';

export default class SearchEngineService {
  apiUrl = API_URL;
  errors = [];

  async getSearchEngines() {
    try {
      let response = await fetch(`${this.apiUrl}/search-engine`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getSearchEngine(id) {
    try {
      let response = await fetch(`${this.apiUrl}/search-engine/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }
}
