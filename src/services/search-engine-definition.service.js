import { API_URL } from '../constants/api.constant';

export default class SearchEngineDefinitionService {
  apiUrl = API_URL;
  errors = [];

  async getSearchEngineDefinitions() {
    try {
      let response = await fetch(`${this.apiUrl}/search-engine-definition`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getSearchEngineDefinition(id) {
    try {
      let response = await fetch(`${this.apiUrl}/search-engine-definition/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }
}
