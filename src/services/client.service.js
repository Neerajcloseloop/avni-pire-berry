import { API_URL } from '../constants/api.constant';

export default class ClientService {
  apiUrl = API_URL;
  errors = [];

  async getClients() {
    try {
      let response = await fetch(`${this.apiUrl}/client`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getClient(id) {
    try {
      let response = await fetch(`${this.apiUrl}/client/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async addClient(data) {
    try {
      let response = await fetch(`${this.apiUrl}/client`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (response.status === 201) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async updateClient(data) {
    try {
      let response = await fetch(`${this.apiUrl}/client/${data.clientId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async deleteClient(data) {
    try {
      let response = await fetch(`${this.apiUrl}/client/${data.clientId}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }
}
