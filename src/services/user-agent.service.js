import { API_URL } from '../constants/api.constant';

export default class UserAgentService {
  apiUrl = API_URL;
  errors = [];

  async getUserAgents() {
    try {
      let response = await fetch(`${this.apiUrl}/user-agent`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getUserAgent(id) {
    try {
      let response = await fetch(`${this.apiUrl}/user-agent/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }
}
