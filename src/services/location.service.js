import { API_URL } from '../constants/api.constant';

export default class LocationService {
    apiUrl = API_URL;

    async getLocationsAutocomplete(location) {
      try {
        let response = await fetch(`${this.apiUrl}/location/search?location=${encodeURI(location)}`, {
          method: 'get'
        });
        if (response.status === 200) {
          return await response.json();
        }
        return [];
      } catch (e) {
        console.log(e);
        return [];
      }
    }
  }
