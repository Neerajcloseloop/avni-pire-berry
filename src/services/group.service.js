import { API_URL } from '../constants/api.constant';

export default class GroupService {
  apiUrl = API_URL;
  errors = [];

  async getGroups() {
    try {
      let response = await fetch(`${this.apiUrl}/group`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getGroupsByClient(id) {
    try {
      let response = await fetch(`${this.apiUrl}/group/client/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getGroup(id) {
    try {
      let response = await fetch(`${this.apiUrl}/group/${id}`, {
        method: 'get'
      });
      if (response.status === 200) {
        return await response.json();
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async addGroup(data) {
    try {
      let response = await fetch(`${this.apiUrl}/group`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (response.status === 201) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async updateGroup(data) {
    try {
      let response = await fetch(`${this.apiUrl}/group/${data.groupId}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = errors.errors;
      return false;
    } catch (e) {
      console.log(e);
    }
  }

  async deleteGroup(data) {
    try {
      let response = await fetch(`${this.apiUrl}/group/${data.groupId}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (response.status === 200) {
        return true;
      }

      const errors = await response.json();
      this.errors = [errors.message];
      return false;
    } catch (e) {
      console.log(e);
    }
  }
}
