import React from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Typography, Grid } from '@mui/material';
import Image from 'next/image';

const FilterActions = ({ title, link, icon }) => {
    const theme = useTheme();

    return (
        <Grid style={{ display: 'flex', padding: "5px", borderRadius: "5px", color: "rgba(74, 133, 251, 0.32)", backgroundColor: "rgba(232, 233, 236, 1)", height: "40px" }}>
            <Grid style={{ display: 'flex', marginRight: "10px", alignItems: 'center' }}>

                <Image alt="Landscape picture"
                    width={20}
                    height={20}
                    marginRight="5px"
                    src="/images/icons/check.svg" />
                <Typography style={{ display: 'flex', marginLeft: "5px", alignItems: 'center' }} align="left" variant="subtitle1" component="div">
                    {'Completed'}{' '}
                </Typography>
            </Grid>
            <Grid style={{ display: 'flex', marginRight: "10px", alignItems: 'center' }}>
                <Image alt="Landscape picture"
                    width={20}
                    height={20}
                    marginRight="5px"
                    src="/images/icons/priority.svg" />
                <Typography style={{ display: 'flex', marginLeft: "5px", alignItems: 'center' }} align="left" variant="subtitle1" component="div">

                    {'Priority'}{' '}
                </Typography>
            </Grid>
            <Grid style={{ display: 'flex', marginRight: "10px", alignItems: 'center' }}>
                <Image alt="Landscape picture"
                    width={20}
                    height={20}
                    marginRight="5px"
                    src="/images/icons/new.svg" />
                <Typography style={{ display: 'flex', marginLeft: "5px", alignItems: 'center' }} align="left" variant="subtitle1" component="div">
                    {'New'}{' '}
                </Typography>
            </Grid>
            <Grid style={{ display: 'flex', marginRight: "10px", alignItems: 'center' }}>
                <Image alt="Landscape picture"
                    width={20}
                    height={20}
                    marginRight="5px"
                    src="/images/icons/started.svg" />
                <Typography style={{ display: 'flex', marginLeft: "5px", alignItems: 'center' }} align="left" variant="subtitle1" component="div">
                    {'started'}{' '}
                </Typography>
            </Grid>
            <Grid style={{ display: 'flex', marginRight: "10px", alignItems: 'center' }}>
                <Image alt="Landscape picture"
                    width={20}
                    height={20}
                    marginRight="5px"
                    src="/images/icons/alert.svg" />
                <Typography style={{ display: 'flex', marginLeft: "5px", alignItems: 'center' }} align="left" variant="subtitle1" component="div">
                    {'Alert'}{' '}
                </Typography>
            </Grid>           
        </Grid>
    );
};

export default FilterActions;
