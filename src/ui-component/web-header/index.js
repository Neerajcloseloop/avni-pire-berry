import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';

export default function WebHeader() {
    const router = useRouter();
    const path = router.asPath;

    const webTitle = React.useMemo(() => {
        if (path) {
            return 'Pire Admin';
        }
    }, [path]);

    return (
        <Head>
            <title>{webTitle}</title>
            <link rel="icon" href="/favicon.svg"></link>
            {/*  Meta Tags */}
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <meta name="theme-color" content="#2296f3"></meta>
                        <link rel="preconnect" href="https://fonts.gstatic.com" />
            <link
                href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Poppins:wght@400;500;600;700&family=Roboto:wght@400;500;700&display=swap"
                rel="stylesheet"
            />
        </Head>
    );
}
