import React, { useCallback, useEffect, useState } from 'react';
import { Button, Grid, Card, Box, Typography } from '@mui/material';
import { gridSpacing } from '../../store/constant';

import Table from '../missions-list/mission-list';
import MissionService from '../../services/mission.service';;


export default function MissionActivity({type=1, title}) {
    const missionService = new MissionService();
    const [allMissions, setMissions] = useState([]);
    const [filteredMissions, setFilteredMissions] = useState([]);
    const [filter, setFilter] = useState('Started');
    
    useEffect(() => {
        const getMissions = async () => {
          const missions = await missionService.getMissions();
          getAndSetFilteredMissions(missions);
          setMissions(missions);
        };
    
        getMissions();
      }, []);

      const getAndSetFilteredMissions=(all_missions)=>{
        if(all_missions && all_missions.length > 0){
            let completedMissions = all_missions.filter(mission=> mission.status === 'Completed');
            if(completedMissions && completedMissions.length>0){
              completedMissions= completedMissions.sort(function(a,b){
                var c =new Date(a.missionStatus.lastRunDateTime);
                var d= new Date(b.missionStatus.lastRunDateTime);
                return d-c;
              }).slice(0,50);
            }
            
            let inProgressMissions = all_missions.filter(mission=> mission.status === 'New').slice(0,5);
    
            let nextMissions = all_missions.filter(mission=> mission.status === 'Pending' && 
            mission.stopMission === false && mission.schedule !== 'Once');
    
            if(nextMissions.length > 0){
              nextMissions= nextMissions.sort(function(a,b){
                var c =(a['nextRunDateTime']);
                var d= (b['nextRunDateTime']);
                return c > d ? 1: -1;
              }).slice(0,50);
            }
            setFilteredMissions(filter === 'Started' ? inProgressMissions : filter === 'Upcomming' ? nextMissions: completedMissions);
          }
      }

    const handleClientFilterChange = (selectedFilter) => {
        setFilter(selectedFilter);
    };

    useEffect(()=>{
        getAndSetFilteredMissions(allMissions)
    }, [filter])

    // table header
    const headCells = [
        {
            id: 'priority',
            numeric: false,
            disablePadding: false,
            label: ''
        },
        {
            id: 'name',
            numeric: false,
            disablePadding: false,
            label: 'MISSION'
        },
        {
            id: 'client.name',
            numeric: false,
            disablePadding: false,
            label: 'CLIENT'
        },
        {
            id: 'group.name',
            numeric: false,
            disablePadding: false,
            label: 'PROGRESS'
        },
    ];

    return (
        <>     
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12} md={12} lg={12}>
                    <Table
                        headerText={title}
                        data={filteredMissions || []}
                        handleClientFilterChange={handleClientFilterChange}
                        columns={headCells}
                        type={3}
                        filter={filter} />
                </Grid>
            </Grid>
        </>
    );
}