import React, {useEffect, useState} from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Autocomplete, Grid, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Stack, TextField, Typography } from '@mui/material';
import ClientService from '../../services/client.service';
import ClientValidator from '../../validators/client.validator';
// project imports
import AnimateButton from '../../ui-component/extended/AnimateButton';
import SaveIcon from '@mui/icons-material/Save';

export default function ClientAddModal({isOpen, type=0, title='Favorite a Client', setClose, clientOptions:clientOptionsAsProps=[], filter=[], handleAddClientFilter}) {
    const theme = useTheme();
    const [clientFilter, setClientFilter]= useState([]);    
    const [clientOptions, setClientOptions]= useState([]);
    const [clientValue, setClientValue]= useState({});
    const options = clientOptionsAsProps.map(v=>{return{id: v.clientId, label: v.name}});
    
    const clientService = new ClientService();
      const clientValidator = new ClientValidator();

    useEffect(()=>{
        setClientOptions(filter && filter.length === 3 ? [] : options)
    }, [clientOptionsAsProps])
    
    const handleClose = () => {
        setClientFilter(filter)
        setClose(false);
    };
    const handleAddClick=()=>{
        let idsArray = clientFilter && clientFilter.length > 0 && clientFilter.map(v=>v.id) || [];
       handleAddClientFilter(idsArray, clientFilter);
       setClose(false);
    }

    const handleOnchange=(event, values)=>{  
        let idsArray = values && values.length > 0 && values.map(v=>v.id) || [];    
        if(idsArray.length > 3){
            setClientOptions(clientOptions.filter(v => idsArray.indexOf(v.id) === 'no'))         
        }else{
            setClientOptions(options.filter(v => idsArray.indexOf(v.id) === -1))
            setClientFilter(values)
        }      
    }

    const handleSave = async (event) => {
        if (!clientValidator.isValid(clientValue)) {
          console.log('test error message to set')
        } else {
          let response = false;
          if (clientValue) {
            response = await clientService.addClient(clientValue);
          } else {
           // response = await clientService.updateClient(clientValue);
          }
          if (response) {
            setClose();
          }
        }
      };

    return (
        <div>
            <Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title" >
                {isOpen && (
                    <>
                        <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                        <DialogContent>
                            {type !== 1 && <Stack spacing={3}>
                                <DialogContentText>
                                    <Typography variant="body2" component="span">
                                        Favorited a clients will appear under "My Missions" section. You can choose upto 3 cleints.
                                    </Typography>
                                </DialogContentText>
                                <Autocomplete
                                    multiple
                                    onChange={(event, values) => handleOnchange(event, values)}
                                    options={clientOptions}
                                    getOptionLabel={(option) => { return option.label }}
                                    defaultValue={clientFilter}
                                    renderInput={(params) => <TextField {...params} />}
                                    onDelete={() => { console.log('test delete') }}
                                />
                            </Stack>}
                            {
                                type === 1 &&
                                <Stack spacing={3}>
                                    <Grid item xs={12} md={12}>
                                        <TextField
                                            id="client"
                                            name="client"
                                            onChange={(e) => { setClientValue({name: e.target.value, clientId: 0}) }}
                                            error={false}
                                            label="Client"
                                            fullWidth
                                            style={{ width: '400px' }}
                                        />
                                    </Grid>
                                </Stack>
                            }
                        </DialogContent>
                        <DialogActions sx={{ pr: 2.5 }}>
                            <Button onClick={handleClose} variant="contained" style={{ marginTop: 'none !important', marginBottom: '0px !important', backgroundColor: '#6c757d', borderColor: '#6c757d' }} sx={{ my: 3, ml: 1 }}>
                                Cancel
                            </Button>
                            <AnimateButton>
                                <Button onClick={type == 1 ? handleSave : handleAddClick} startIcon={<SaveIcon />} sx={{ marginTop: 'none !important', marginBottom: '0px !important' , my: 3, ml: 1 }} variant="contained" type="submit" >
                                    {type !== 1 ? 'Add' : 'Save'}
                                </Button>
                            </AnimateButton>
                        </DialogActions>
                    </>
                )}
            </Dialog>
        </div>
    );
}
