import React, {useState, useEffect} from 'react';

// material-ui
import {
    Box,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableSortLabel,
    TableRow,
    Button,
    Grid,
    TextField
} from '@mui/material';
import { visuallyHidden } from '@mui/utils';

import AddIcon from '@mui/icons-material/Add';
import { useTheme } from '@mui/material/styles';

import { gridSpacing } from '../../store/constant';
// project imports
import MainCard from '../../ui-component/cards/MainCard';
import { IconSearch, IconEdit } from '@tabler/icons';
import Link from 'next/link';
// table filter
function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return (a[1]) - (b[1]);
    });
    return stabilizedThis.map((el) => el[0]);
}

function EnhancedTableHead({ columns, order, orderBy, onRequestSort }) {
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {columns.map((headCell) => {
                    return (
                        <TableCell
                            key={headCell.id}
                            align={headCell.numeric ? 'right' : 'left'}
                            padding={headCell.disablePadding ? 'none' : 'normal'}
                            sortDirection={orderBy === headCell.id ? order : undefined}
                        >
                            <TableSortLabel
                                active={orderBy === headCell.id}
                                direction={orderBy === headCell.id ? order : 'asc'}
                                onClick={createSortHandler(headCell.id)}
                            >
                                {headCell.label}
                                {orderBy === headCell.id ? (
                                    <Box component="span" sx={visuallyHidden}>
                                        {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                    </Box>
                                ) : null}
                            </TableSortLabel>
                        </TableCell>)
                }
                )}
            </TableRow>
        </TableHead>
    );
}

const RenderTable = ({ data = [], columns,  onButtonClick, type, }) => {
    const [order, setOrder] = useState('asc');
    const [search, setSearch] = useState('');
    const [sort, setSort] = useState('name');

    const [orderBy, setOrderBy] = useState('');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [dense] = React.useState(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const [rows, setData] = useState([]);

    useEffect(()=>{
        setData(data);
    }, [data])

    const getClientFilteredData = async () => {
        let clientsData = data;
        if (search !== '' && search.length > 0) {
          clientsData = clientsData.filter((x) => x.name.toLowerCase().includes(search.toLowerCase()));  
        }    
        clientsData.sort((a, b) => {
            return a[sort].toLowerCase() > b[sort].toLowerCase() ? order === 'asc' ? 1 : -1 : order === 'asc' ? 1 : -1 * -1;
          });    
        setData(clientsData);
       
    
      }
      useEffect(() => {
        getClientFilteredData();
      }, [search, sort]);


      const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setSort(property);
        setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelectedId = rows.map((n) => n.name);
            setSelected(newSelectedId);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
        }
        setSelected(newSelected);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event?.target.value));
        setPage(0);
    };

    const isSelected = (name) => selected.indexOf(name) !== -1;

    const renderActionButton = () => {
        return (
            <Button variant="contained" startIcon={<AddIcon />} onClick={onButtonClick}>
                Add Client
            </Button>)
    }

    
  
    return (
        <MainCard
            content={false}
            title={
                <Grid container alignItems="center"  spacing={gridSpacing}>                  
                    <Grid item>                        
                        <TextField iconPrimary={IconSearch}  variant="standard" placeholder="search clients" onChange={(e)=>{setSearch(e.target.value)}}/>
                    </Grid>
                </Grid>
            }
            secondary={<div style={{ display: 'flex', alignItems: 'center' }}>{renderActionButton()}</div>}
        >
           
            {/* table */}
            <TableContainer>
                <Table sx={{ minWidth: 550 }} aria-labelledby="tableTitle" size={type === 3 ? 'small' : 'medium'}>
                    <EnhancedTableHead
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={rows.length}
                        columns={columns}
                    />
                    <TableBody>
                        {rows && rows.length > 0 ?
                            stableSort(rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    if (typeof row === 'number') return null;
                                    const isItemSelected = isSelected(row.name);
                                    return (
                                        <TableRow
                                            hover
                                            onClick={(event) => handleClick(event, row.name)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.name}
                                            selected={isItemSelected}
                                        >

                                            <TableCell align="left">
                                                <Link href={`/client/${row.clientId}`}>
                                                    <a className='text-dark'>
                                                     <IconEdit stroke={2} size="1.5rem" sx={{pointer: 'cursor'}} />
                                                    </a>
                                                </Link>
                                             {row.name}
                                            </TableCell>                                            
                                           
                                        </TableRow>
                                    );
                                })

                            : <TableRow
                                style={{
                                    height: (dense ? 33 : 53) * 3,

                                }}
                            >
                                {type !== 3 && <TableCell></TableCell>}
                                {type !== 3 && <TableCell></TableCell>}
                                {<TableCell></TableCell>}
                                {<TableCell></TableCell>}
                                <TableCell align="left" colSpan={type !== 3 ? 9 : 3}> No Records Found </TableCell>
                            </TableRow>
                        }
                    </TableBody>
                </Table>
            </TableContainer>

            {/* table pagination */}
            <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </MainCard>
    );
}


export default RenderTable;
