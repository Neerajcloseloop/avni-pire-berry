import React, { useCallback, useEffect, useState } from 'react';
// mui
import { useTheme } from '@mui/material/styles';
import { Button, Grid, Card, Box, Typography ,Autocomplete, TextField} from '@mui/material';
import { gridSpacing } from '../../store/constant';
import AddIcon from '@mui/icons-material/Add';

import Table from './mission-list';
import MissionService from '../../services/mission.service';
import ClientService from '../../services/client.service';
import GroupService from '../../services/group.service';
import { useAppContext } from '../../context/app.context';
import { IconSearch, IconEdit } from '@tabler/icons';

export default function MissionsList({type=1, missions: missionsAsProp=[]}) {
    const theme = useTheme();
    const missionService = new MissionService();
    const { state, actions } = useAppContext();
    const clientService = new ClientService();
    const groupService = new GroupService();
    const [clients, setClients] = useState([]);
    const [groups, setGroups] = useState([]);
    const [groupOptions, setGroupOptions] = useState([]);
    const [missions, setMissions] = useState(missionsAsProp);
    const [filter, setFilter] = useState({ client: state.client, group: state.group });
    const [search, setSearch] = useState('');
    const [myMissions, setMyMissions] = useState([]);
  
    useEffect(() => {
        if (type === 2) {
            let myMission=[];
            if (filter.client.length > 0) {
                myMission = missions.filter((x) =>  filter.client.indexOf(x.clientId) > -1);
            }else{
                myMission=[]  
            }         
            setMyMissions(myMission);
        }

    }, [filter,  filter.client]);

    const getMissions = useCallback(async () => {
        const missions = type === 4 ? missionsAsProp : await missionService.getMissions();
        if (search.length > 0) {
            missions = missions.filter(
              (x) =>
                x.name.toLowerCase().includes(search.toLowerCase()) ||
                x.client.name.toLowerCase().includes(search.toLowerCase()) ||
                x.group.name.toLowerCase().includes(search.toLowerCase()) ||
                x.status.toLowerCase().includes(search.toLowerCase())
            );
          }
          if (filter.client > 0) {
            missions = missions.filter((x) => x.clientId === filter.client);
          }
          if (filter.group > 0) {
            missions = missions.filter((x) => x.groupId === filter.group);
          }
        setMissions(missions);
        return missions
    }, [search, filter]);

    useEffect(() => {
     getMissions();
    }, [search, filter]);

    useEffect(() => {
        const getClients = async () => {
          const clients = await clientService.getClients();
          clients.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1));
          setClients(clients);
        };
    
        const getGroups = async () => {
          const groups = await groupService.getGroups();
          groups.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1));
          setGroups(groups);
        };
    
        getClients();
        getGroups();
      }, [missions]);

      const handleSearchChange = (event) => {
        setSearch(event.target.value);
      };
    
    const handleClientFilterChange = (selectedClients) => {
        setFilter({ client: selectedClients, group: 0 });
    };

    const handleClientFilter = (eventId) => {
        const clientOption = eventId
        setFilter({ client: clientOption, group: 0 });
        actions.setClient(clientOption);
        actions.setGroup(0);
        let groupOptions = Array.from(groups);
        groupOptions.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1));
        if (clientOption > 0) {
          groupOptions = groupOptions.filter((x) => x.clientId === clientOption);
        }
        setGroupOptions(groupOptions);
      };

    const handleGroupFilterChange = (eventId = 0) => {
        if (eventId > 0) {
            const group = groups.find((x) => x.groupId === eventId);
            setFilter({ client: group.clientId, group: eventId });
            actions.setClient(group.clientId);
            actions.setGroup(eventId);
        } else {
            setFilter({ client: 0, group: eventId });
        }
    };


    // table header
    const headCells = [
        {
            id: 'priority',
            numeric: false,
            disablePadding: false,
            label: 'STATUS'
        },
        {
            id: 'name',
            numeric: false,
            disablePadding: false,
            label: 'MISSION'
        },
        {
            id: 'client.name',
            numeric: false,
            disablePadding: false,
            label: 'CLIENT'
        },
        {
            id: 'group.name',
            numeric: false,
            disablePadding: false,
            label: 'GROUP'
        },
        {
            id: 'schedule',
            numeric: false,
            disablePadding: false,
            label: 'SCHEDULE'
        },
        {
            id: 'missionStatus.lastRunDateTime',
            numeric: false,
            disablePadding: false,
            label: 'LAST RUN'
        },
        {
            id: 'searchTermCount',
            numeric: false,
            disablePadding: false,
            label: 'SEARCH TERMS'
        },
        {
            id: 'status',
            numeric: false,
            disablePadding: false,
            label: 'STATUS'
        },


        {
            id: 'missionStatus.totalNumMessages',
            numeric: false,
            disablePadding: false,
            label: 'PROGRESS'
        },
        {
            id: 'nextRunDateTime',
            numeric: false,
            disablePadding: false,
            label: 'NEXT RUN'
        }
    ];

    const tableHeader1 = () => {
        return (
            <Card
                sx={{
                    marginBottom: theme.spacing(gridSpacing),
                    border: '1px solid',
                    borderColor: theme.palette.background.default,
                    background: theme.palette.background.default
                }}
            >
                <Box sx={{ p: 2, pl: 2 }}>
                    <Grid
                        container
                        direction={'row'}
                        justifyContent={'space-between'}
                        alignItems={'center'}
                        spacing={1}
                    >
                        <Grid item>
                            <Typography variant="h3" sx={{ fontWeight: 500 }}>
                                {'All Missions'}
                            </Typography>
                        </Grid>
                        <Button variant="contained" endIcon={<AddIcon />} onClick={() => window.location.href = '/add-missions'}>
                            Add Mision
                        </Button>
                    </Grid>

                </Box>
            </Card>)
    }

    const tableHeader4 = () => {
        return (
            <Card
                sx={{
                    marginBottom: theme.spacing(gridSpacing),
                    border: '1px solid',
                    borderColor: theme.palette.background.default,
                    background: theme.palette.background.default
                }}
            >
                <Box sx={{ p: 2, pl: 2 }}>
                    <Grid
                        container
                        direction={'row'}
                        justifyContent={'space-between'}
                        alignItems={'center'}
                        spacing={1}
                    >
                        <Grid xs={9} item>
                            <Grid xs={12} container alignItems="center" spacing={gridSpacing}>
                                <Grid item xs={4}>
                                    <TextField startIcon={IconSearch} value={search}
                                        fullWidth id="filled-basic"
                                        label="Search Missions"
                                        variant="standard"
                                        onChange={handleSearchChange} />
                                </Grid>
                                <Grid item xs={4}>
                                    <Autocomplete
                                        options={clients?.length > 0
                                            ? clients.map((client, index) => (
                                                { label: client.name, id: client.clientId }
                                            ))
                                            : []}
                                        //  value={filter.client !==0 ? filter.client : ''}
                                        onChange={(event, value) => {handleClientFilter(value?.id)}}
                                        renderInput={(params) => <TextField {...params} label="Filter Client" />}
                                    />
                                </Grid>
                                <Grid item  xs={4}>
                                    <Autocomplete
                                        options={groupOptions?.length > 0
                                            ? groupOptions.map((group, index) => (
                                                { label: group.name, id: group.groupId }
                                            ))
                                            : []}
                                        //value={filter.group !==0 ? filter.group : ''}
                                        onChange={(event, value) => {handleGroupFilterChange(value?.id)}}
                                        renderInput={(params) => <TextField {...params} label="Filter Group" />}
                                    />
                                </Grid>

                            </Grid>
                        </Grid>
                        <Button variant="contained" endIcon={<AddIcon />} onClick={() => window.location.href = '/add-missions'}>
                            Add Mission
                        </Button>
                    </Grid>
                </Box>
            </Card>)
    }


    return (
        <>     
            {type === 1 && tableHeader1()}      
            {type === 4 && tableHeader4()}      
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Table 
                      headerText={ type===1 ? "All Active Pire Missions" : type !== 4 ? "My Missions" : ''} 
                      data={ (type===1 || type== 4) ? missions : myMissions || []} 
                      columns={headCells} 
                      type={type} 
                      filter={filter}
                      clients={clients} 
                      handleClientFilterChange={handleClientFilterChange}
                    />
                </Grid>
            </Grid>
        </>
    );
}