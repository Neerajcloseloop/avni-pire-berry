import * as React from 'react';

// material-ui
import {
    Box,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableSortLabel,
    TableRow,
    Card,
    Button,
    Grid,
    Typography,
    Divider
} from '@mui/material';
import { visuallyHidden } from '@mui/utils';

import AddIcon from '@mui/icons-material/Add';
import { useTheme } from '@mui/material/styles';

// project imports
import MainCard from '../../ui-component/cards/MainCard';

import moment from 'moment'
import Image from 'next/image';
import { gridSpacing } from '../../store/constant';
import FilterActions from '../../ui-component/cards/CardSecondaryAction';
import ClientAddModal from '../client-add-modal';
import InfoSharpIcon from '@mui/icons-material/InfoSharp';
import MuiTooltip, { tooltipClasses, TooltipProps } from '@mui/material/Tooltip';

// import styles from './mission-style.module.scss';


// table filter
function descendingComparator(a, b, orderBy) {
    /*
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
    */

    switch (orderBy) {
        case 'client.name':
          return a.client.name.toLowerCase() > b.client.name.toLowerCase() ? 1 : -1;
        case 'group.name':
          return a.group.name.toLowerCase() > b.group.name.toLowerCase() ? 1 : -1;
        case 'missionStatus.lastRunDateTime':
          if (a.missionStatus === null) {
            return -1;
          }
          if (b.missionStatus === null) {
            return 1;
          }
          return a.missionStatus.lastRunDateTime > b.missionStatus.lastRunDateTime ? 1 : -1;
        case 'missionStatus.totalNumMessages':
          if (a.missionStatus === null) {
            return -1;
          }
          if (b.missionStatus === null) {
            return 1;
          }
          return a.missionStatus.totalMessages > b.missionStatus.totalMessages ? 1 : -1;
        case 'nextRunDateTime':
            if (a.stopMission === true) {
                return -1;
              }
              if (b.stopMission === true) {
                return 1;
              }
              if (a.schedule === 'Once') {
                return -1;
              }
              if (b.schedule === 'Once') {
                return 1;
              }

              return a.nextRunDateTime > b.nextRunDateTime ? 1 : -1;
        case 'priority':
        case 'searchTermCount':
          return a[orderBy] > b[orderBy] ? 1 : -1;
        default:
          return a[orderBy] > b[orderBy] ? 1 : -1;
      }
}

function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return (a[1]) - (b[1]);
    });
    return stabilizedThis.map((el) => el[0]);
}

function EnhancedTableHead({ columns, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort }) {
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {columns.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : undefined}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

export default function EnhancedTable({ data: rows, columns, headerText, handleClientFilterChange, type, filter, clients }) {
    const theme = useTheme();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [dense] = React.useState(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [clientFilterToShow, setClientFilter] = React.useState([]);
    
    const [isModalOpen, setModalState] = React.useState(false);

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelectedId = rows.map((n) => n.name);
            setSelected(newSelectedId);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
        }
        setSelected(newSelected);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event?.target.value));
        setPage(0);
    };

    const isSelected = (name) => selected.indexOf(name) !== -1;

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const handleAddClientFiltter = (idsArr, clientFilter) => {
        handleClientFilterChange(idsArr);
        clientFilter.sort((a,b)=> { return a.label.toLowerCase() > b.label.toLowerCase() ? 1 : -1});
        setClientFilter(clientFilter);
    }

    const tableHeader2 = () => {
        return (
            <Card
                sx={{
                    marginBottom: theme.spacing(gridSpacing),
                    border: '1px solid',
                    borderColor: theme.palette.background.default,
                    background: theme.palette.background.default,
                    marginBottom: '2px'
                }}
            >
                <Box sx={{ p: 2, pl: 2 }}>
                    <Grid
                        container
                        direction={'row'}
                        justifyContent={'space-between'}
                        alignItems={'center'}
                        spacing={1}
                    >
                        <Grid item>
                            <Typography variant="h4" sx={{ fontWeight: 500,}}>
                                {clientFilterToShow.map(v => <span style={{ marginRight: '10px', paddingBottom: '10px', borderBottom: '2px solid' ,  color: '#2196f3',  }}>{v.label}</span>)}
                            </Typography>
                        </Grid>
                        {actionButton()}
                    </Grid>
                </Box>
                <Divider />
            </Card>)
    }

    const tableHeader3 = () => {
        return (
            <Card
                sx={{
                    marginBottom: theme.spacing(gridSpacing),
                    border: '1px solid',
                    borderColor: theme.palette.background.default,
                    background: theme.palette.background.default,
                    marginBottom: '2px'
                }}
            >
                <Box sx={{ p: 2, pl: 2 }}>
                    <Grid
                        container
                        direction={'row'}
                        justifyContent={'space-between'}
                        alignItems={'center'}
                        spacing={1}
                    >
                        <Grid item>
                            <Typography variant="h4" sx={{ fontWeight: 500,}}>
                                {['Started','Upcomming','Completed'].map(v => <span style={{ marginRight: '10px', cursor: 'pointer', paddingBottom: '10px',borderBottom: filter === v ? '2px solid' : 'none' ,  color: filter === v ? '#2196f3' : '#000', fontWeight: 'bold' }} onClick={()=>handleClientFilterChange(v)} >{v}</span>)}
                            </Typography>
                        </Grid>
                    </Grid>
                </Box>
                <Divider />
            </Card>)
    }

    const handleModalOpen = () => {
        setModalState(!isModalOpen);
    };

    const actionButton = () => <Button variant="contained" startIcon={<AddIcon />} onClick={handleModalOpen}>
        Add Client Shortcut
    </Button>

    const textToDisplay= "Displays the mission that is currently being searched by the system along with percent remainging at the keywords/ASINS being searched.You can also see this mission listed in the `started` column under `all PIRE missions`"

    return (
        <MainCard
            content={false}
            title={headerText}
            secondary={<div style={{ display: 'flex', alignItems: 'center' }}>
                {type !==3 && <FilterActions />}
                {type !==1 && <MuiTooltip  title={textToDisplay} arrow  placement="left-start" >
                    <InfoSharpIcon style={{
                        color: 'rebeccapurple',
                        cursor: 'pointer',
                        marginLeft: '230px',
                    }} /></MuiTooltip>} </div>}
        >
            {<ClientAddModal filter={clientFilterToShow} clientOptions={clients} isOpen={isModalOpen} setClose={handleModalOpen}
                handleAddClientFilter={handleAddClientFiltter} />}

            {type === 2 ? tableHeader2() : type ==3 && tableHeader3()}

            {/* table */}
            <TableContainer>
                <Table sx={{ minWidth: 550 }} aria-labelledby="tableTitle" size={type === 3 ? 'small' : 'medium'}>
                    <EnhancedTableHead
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={rows.length}
                        columns={columns}
                    />
                    <TableBody>
                        {rows && rows.length > 0 ?
                            stableSort(rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    if (typeof row === 'number') return null;
                                    const isItemSelected = isSelected(row.name);
                                    const isStarted = ((row?.missionStatus?.completedMessages / row?.missionStatus?.totalNumMessages) * 100).toFixed(1) < 100
                                    return (
                                        <TableRow
                                            hover
                                            onClick={(event) => handleClick(event, row.name)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.name}
                                            selected={isItemSelected}
                                        >

                                            <TableCell align="left">
                                                {row.status === 'Completed' ?
                                                    <Image alt="Landscape picture"
                                                        className='filterIcon'
                                                        width={20}
                                                        height={20}
                                                        src="/images/icons/check.svg" />
                                                    : row.status === 'New' ? <Image alt="Landscape picture"
                                                        width={20}
                                                        height={20}
                                                        marginRight="5px"
                                                        src="/images/icons/new.svg" /> : ''
                                                }
                                                {row.priority &&
                                                    <Image alt="Landscape picture"
                                                        width={20}
                                                        height={20}
                                                        marginRight="5px"
                                                        src="/images/icons/priority.svg" />

                                                }
                                                {
                                                    isStarted &&
                                                    <Image alt="Landscape picture"
                                                        width={20}
                                                        height={20}
                                                        marginRight="5px"
                                                        src="/images/icons/started.svg" />
                                                }
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.name}
                                            </TableCell>
                                            <TableCell align="left">{row.client.name}</TableCell>
                                           {type !==3 && <TableCell align="left">{row.group.name}</TableCell>}
                                           {type !==3 && <TableCell align="left">{row.schedule}</TableCell>}
                                               {type !==3 &&<TableCell sx={{ pr: 3 }} align="left">
                                                {row.missionStatus !== null ? moment(row.missionStatus.lastRunDateTime).format('MMMM Do YYYY, h:mm a') : null}
                                            </TableCell>}
                                            <TableCell align="left">{row.searchTermCount}</TableCell>
                                            <TableCell align="left">{row.status}</TableCell>
                                            <TableCell> {row.missionStatus !== null ? (
                                                <span>
                                                    {((row.missionStatus.completedMessages / row.missionStatus.totalNumMessages) * 100).toFixed(1)}%
                                                    <br />
                                                    ({row.missionStatus.completedMessages}/{row.missionStatus.totalNumMessages})
                                                </span>
                                            ) : null}</TableCell>
                                               {type !==3 &&<TableCell align="left">{row.stopMission === true || row.schedule === 'Once' ? '' : moment(row.nextRunDateTime).format('MMMM Do YYYY, h:mm a')}
                                            </TableCell>}
                                        </TableRow>
                                    );
                                })

                            : <TableRow
                                style={{
                                    height: (dense ? 33 : 53) * 3,

                                }}
                            >
                                {type !== 3 && <TableCell></TableCell>}
                                {type !== 3 && <TableCell></TableCell>}
                                {<TableCell></TableCell>}
                                {<TableCell></TableCell>}
                                <TableCell align="left" colSpan={type !== 3 ? 9 : 3}> No Records Found </TableCell>
                            </TableRow>
                        }


                        {emptyRows > 0 && (
                            <TableRow
                                style={{
                                    height: (dense ? 33 : 53) * emptyRows
                                }}
                            >
                                <TableCell colSpan={6}> No Records Found </TableCell>
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>

            {/* table pagination */}
            <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </MainCard>
    );
}
