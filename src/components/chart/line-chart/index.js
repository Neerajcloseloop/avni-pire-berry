import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  interaction: {
    mode: 'index',
    intersect: false,
  },
  stacked: false,
  plugins: {
    title: {
      display: false,
      text: '',
    },
    legend:{
      display: false
    }
  },
  scales: {
    y: {
      type: 'linear',
      display: true,
      position: 'left',
    },
  },
};

const labels = ['Feb 15', 'Feb 16', 'Feb 17', 'Feb 18',];

export const data = {
  labels,
  datasets: [
    {
      label: '',
      data: [7, 8, 11, 5],
      borderColor: '#03045e',
      backgroundColor: '#03045e',
      yAxisID: 'y',
    }
  ],
};

export function LineChart() {
  return <Line options={options} data={data} />;
}
