import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

 const options = {
  plugins: {
    title: {
      display: false,
      text: '',
    },
    legend:{
      display: false
    }
  },
  responsive: true,
  scales: {
    x: {
      stacked: true,
    },
    y: {
      stacked: true,
    },
  },
};


const labels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];

 const data = {
  labels,
  datasets: [
    {
      label: 'Dataset 1',
      data: [5,7, 8, 11],
      backgroundColor: '#caf0f8',
    },
    {
      label: 'Dataset 2',
      data: [7,11, 3, 11],
      backgroundColor: '#0077b6',
    },
    {
      label: 'Dataset 3',
      data: [3,9, 5, 8],
      backgroundColor: '#03045e',
    },
  ],
};

const BoxBarChart =()=> {
  return <Bar options={options} data={data} />;
}

export default BoxBarChart;