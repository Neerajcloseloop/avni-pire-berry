let apiUrl = '';

const hostname = typeof location !== 'undefined' ? window.location.hostname : '';

switch (hostname) {
  case 'pire-admin.s3-website-us-east-1.amazonaws.com':
    apiUrl = 'http://pire2-admin-services-prod.eba-2vws6qkm.us-east-1.elasticbeanstalk.com/api';
    break;
  case 'pire-admin-dev.s3-website-us-east-1.amazonaws.com':
    apiUrl = 'http://pire2-admin-services-dev-2.eba-vcepztuh.us-east-1.elasticbeanstalk.com/api';
    break;
  default:
    // apiUrl = 'http://localhost:8080/api';
    apiUrl = 'http://pire2-admin-services-dev-2.eba-vcepztuh.us-east-1.elasticbeanstalk.com/api';
    // apiUrl = 'http://pire2-admin-services-prod.eba-2vws6qkm.us-east-1.elasticbeanstalk.com/api';
    break;
}

export const API_URL = apiUrl;
