// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconBrandChrome, IconBrowser, IconHelp, IconSitemap } from '@tabler/icons';

// constant
const icons = {
    IconBrandChrome,
    IconBrowser,
    IconHelp,
    IconSitemap
};

const other = [
    {
    id: 'sample-docs-roadmap',
    type: 'group',
    title:"GO TO",
    children: [
        {
            id: 'home',
            title: <FormattedMessage id="home" />,
            type: 'item',
            url: '/home',
            icon: icons.IconBrandChrome,
            breadcrumbs: false
        },
        {
            id: 'client',
            title: <FormattedMessage id="clients" />,
            type: 'item',
            url: '/client/list',
            icon: icons.IconBrowser,
            breadcrumbs: false
        },
        {
            id: 'missions',
            title: <FormattedMessage id="missions" />,
            type: 'item',
            url: '/missions',
            icon: icons.IconBrowser,
            breadcrumbs: false
        },
        {
            id: 'domains',
            title: <FormattedMessage id="domains" />,
            type: 'item',
            url: '/domain/list',
            icon: icons.IconBrandChrome,
            breadcrumbs: false
        },        
        
    ]
},
];

export default other;
